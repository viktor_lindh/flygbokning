package travelbooking;

import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class DatabaseConnector { // Class för DB connetion och funktiner för DB
	private static String database = "flight_system";
	private static Connection conn;
	public static Statement stmt;
	private static Scanner input = new Scanner(System.in);
	private static String url = "jdbc:mysql://localhost:3306/";
	private static String user = "root";
	private static String pass = "vl1234";
	
	
	public DatabaseConnector() { // sätter upp connection till DB

		try {

			conn = DriverManager.getConnection(url, user, pass);
			Class.forName("com.mysql.jdbc.Driver");
			stmt = conn.createStatement();

		} catch (ClassNotFoundException | SQLException exc) {
			exc.printStackTrace();

		}
	}
	
	public String databaseSetup() { // Sätter upp databasen.
		
		try {
		
			stmt.executeUpdate("DROP SCHEMA IF EXISTS flight_system;");
			stmt.executeUpdate("CREATE SCHEMA flight_system;");
			stmt.executeUpdate("CREATE TABLE flight_system.airports_departure (airport_id INT NOT NULL AUTO_INCREMENT,"
									+ "airport_name VARCHAR(45) NULL, airport_town VARCHAR(45) NULL,"
									+ "airport_country VARCHAR(45) NULL, PRIMARY KEY (airport_id),"
									+ " UNIQUE INDEX airport_name_UNIQUE (airport_name ASC));");
			stmt.executeUpdate("CREATE TABLE flight_system.airports_destination (airport_id INT NOT NULL AUTO_INCREMENT,"
									+ "airport_name VARCHAR(45) NULL, airport_town VARCHAR(45) NULL,"
									+ "airport_country VARCHAR(45) NULL, PRIMARY KEY (airport_id),"
									+ " UNIQUE INDEX airport_name_UNIQUE (airport_name ASC));");
			stmt.executeUpdate("CREATE TABLE flight_system.airlines (airline_id INT NOT NULL AUTO_INCREMENT,"
									+ "airline_name VARCHAR(45) NOT NULL, airline_country VARCHAR (45) NULL, "
									+ "PRIMARY KEY (airline_id, airline_name), UNIQUE INDEX airline_name_UNIQUE (airline_name ASC));");
			stmt.executeUpdate("CREATE TABLE flight_system.flights (flight_id INT NOT NULL AUTO_INCREMENT,"
									+ " flight_name VARCHAR(5) NULL, flight_departure INT(11) NOT NULL,"
									+ " flight_destination INT(11) NOT NULL, flight_airline INT(11) NULL, "
									+ "departure_date VARCHAR(45) NULL, PRIMARY KEY (flight_id, flight_departure, flight_destination),"
									+ " UNIQUE INDEX flight_name_UNIQUE (flight_name ASC), UNIQUE INDEX departure_date_UNIQUE"
									+ " (departure_date ASC), INDEX FK_flights_departure_idx (flight_departure ASC),"
									+ " INDEX FK_flights_destination_idx (flight_destination ASC),"
									+ " INDEX FK_fligts_airline_idx (flight_airline ASC),"
									+ " CONSTRAINT FK_flights_departure FOREIGN KEY (flight_departure)"
									+ " REFERENCES flight_system.airports_departure (airport_id)"
									+ " ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT FK_flights_destination"
									+ " FOREIGN KEY (flight_destination) REFERENCES flight_system.airports_destination"
									+ " (airport_id) ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT FK_fligts_airline"
									+ " FOREIGN KEY (flight_airline) REFERENCES flight_system.airlines (airline_id)"
									+ " ON DELETE NO ACTION ON UPDATE NO ACTION);");
			stmt.executeUpdate("CREATE TABLE flight_system.bookings (booking_id INT NOT NULL AUTO_INCREMENT,"
									+ " booking_firstname VARCHAR(45) NULL, booking_lastname VARCHAR (45) NULL,"
									+ " booking_price VARCHAR (45) NULL, booking_flight INT(11) NOT NULL, PRIMARY KEY (booking_id, booking_flight),"
									+ " CONSTRAINT FK_booking_flight FOREIGN KEY (booking_flight) REFERENCES flight_system.flights"
									+ " (flight_id) ON DELETE NO ACTION ON UPDATE NO ACTION);");
			stmt.executeUpdate("CREATE TABLE flight_system.customer (customer_id INT NOT NULL AUTO_INCREMENT,"
									+ " customer_firstname VARCHAR(45) NULL, customer_lastname VARCHAR(45) NULL,"
									+ " customer_phonenr VARCHAR(45) NULL, customer_email VARCHAR(45) NULL, PRIMARY KEY"
									+ " (customer_id));");
			}
		catch (Exception exc){
			exc.printStackTrace();
		}
		return "Database deleted.";
	}
	public String ExampleData(){
		try{
			
			/*---------------------------------------------Airports--------------------------------------------------*/
			stmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('1', 'ARN', 'Stockholm', 'Sweden');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('2', 'OSD', 'Ostersund', 'Sweden');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('3', 'AMS', 'Amsterdam', 'Netherlands');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('4', 'LAX', 'Los Angeles', 'USA');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('5', 'BKK', 'Bangkok', 'Thailand');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('6', 'AUC', 'Auckland', 'New Zealand');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('7', 'LHR', 'London', 'Great Britain');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('8', 'BER', 'Berlin', 'Germany');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('9', 'SYD', 'Sydney', 'Australia');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('10', 'RKV', 'Reykjavik', 'Iceland');");
			/*---------------------------------------------Airports--------------------------------------------------*/
			stmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('1', 'ARN', 'Stockholm', 'Sweden');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('2', 'OSD', 'Ostersund', 'Sweden');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('3', 'AMS', 'Amsterdam', 'Netherlands');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('4', 'LAX', 'Los Angeles', 'USA');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('5', 'BKK', 'Bangkok', 'Thailand');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('6', 'AUC', 'Auckland', 'New Zealand');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('7', 'LHR', 'London', 'Great Britain');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('8', 'BER', 'Berlin', 'Germany');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('9', 'SYD', 'Sydney', 'Australia');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('10', 'RKV', 'Reykjavik', 'Iceland');");
			/*---------------------------------------------Airlines--------------------------------------------------*/
			stmt.executeUpdate("INSERT INTO flight_system.airlines (airline_id, airline_name, airline_country) VALUES"
					+ " ('1', 'Viktor Aviation', 'Sweden');");
			stmt.executeUpdate("INSERT INTO flight_system.airlines (airline_id, airline_name, airline_country) VALUES"
					+ " ('2', 'ScandinavianAIR', 'Sweden');");
			stmt.executeUpdate("INSERT INTO flight_system.airlines (airline_id, airline_name, airline_country) VALUES"
					+ " ('3', 'KillerAirWay', 'Great Britain');");
			stmt.executeUpdate("INSERT INTO flight_system.airlines (airline_id, airline_name, airline_country) VALUES"
					+ " ('4', 'TommeTomat', 'Denmark');");
			stmt.executeUpdate("INSERT INTO flight_system.airlines (airline_id, airline_name, airline_country) VALUES"
					+ " ('5', 'RyanAir', 'Germany');");
			/*-----------------------------------------------Flights-------------------------------------------------------*/
			stmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('1', 'SK571', '1', '5', '1', '2015-06-01');");
			stmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('2', 'SK819', '2', '8', '2', '2015-05-01');");
			stmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('3', 'AR912', '3', '1', '3', '2015-05-10');");
			stmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('4', 'GS846', '9', '10', '5', '2015-12-24');");
			stmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('5', 'LU782', '6', '4', '4', '2015-09-15');");
			stmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('6', 'SK123', '8', '7', '3', '2015-12-01');");
			stmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('7', 'SK574', '5', '4', '2', '2015-01-15');");
			stmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('8', 'SK901', '9', '3', '3', '2015-05-03');");
			stmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('9', 'GB666', '3', '2', '1', '2015-11-03');");
			stmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('10', 'KP001', '10', '1', '5', '2015-11-01');");
			stmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('11', 'SD101', '10', '1', '5', '2015-11-02');");
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
		return "Database with example data complete.";
	}
	
	
	public void airportInsert(String airport_name, String airport_town, String airport_country){ // Metod för att föra in nya airports.	
		try {
			
			stmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_name, airport_town, airport_country)"
					+ " VALUES('" + airport_name + "', '" + airport_town + "', '" + airport_country +"');");
			stmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_name, airport_town, airport_country)"
					+ " VALUES('" + airport_name + "', '" + airport_town + "', '" + airport_country +"');");
		}
		catch (Exception exc){
			exc.printStackTrace();
		}

	
	}
	
	public void airlineInsert(String airline_name, String airline_country) { // För in data i airline table i DB

		try {
			stmt.executeUpdate("INSERT INTO flight_system.airlines (airline_name, airline_country) VALUES('" 
												+ airline_name + "', '" + airline_country + "');");
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
	
	public void flightInsert(int airline, String flight_name, int departure, int destination, String departureDate)
			 { // för in data i DB för flights

		try {
			stmt.executeUpdate("INSERT INTO flight_system.flights (flight_name, flight_departure, flight_destination,"
										+ " flight_airline, departure_date) VALUES('" + flight_name + "', '"
										+ departure + "', '" + destination + "', '" + airline + "', '" + departureDate +"');");
		}
		catch (Exception exc){
			exc.printStackTrace();
		}

}
	
	
	public void showAirport() { // Metod som printar ut alla Airports
		try{
		
			ResultSet result = stmt.executeQuery("select * from flight_system.airports_departure");
			while (result.next()){
			System.out.println("("+ result.getString("airport_id") + ") "+ result.getString("airport_town")
					+ " (" + result.getString("airport_name") + ")");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}

	}
	
	public void delAirport() { // Metod som tarbort Airports från DB
		try {

			int delIn = input.nextInt();
			String strSelect = ("DELETE from " + database
					+ ".airport_departure WHERE airport_id LIKE " + delIn);
			String strSelect1 = ("DELETE from " + database
					+ ".airport_destination WHERE airport_id LIKE " + delIn);
			stmt.executeUpdate(strSelect+strSelect1);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	public void showAirline() { // Metod som printar ut alla Airlines
		try{

			ResultSet result = stmt.executeQuery("select * from flight_system.airlines");
			while (result.next()){
			System.out.println("(" + result.getString("airline_id") + ") " + result.getString("airline_name") + "\n" +"Based in: "
										+ result.getString("airline_country") + "\n");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}

	}
	
	
	public void delAirline() { // Metod för att kunna välja och deleta ett Flygbolag
		try {

			int delIn = input.nextInt();
			String strSelect = ("DELETE from " + database
					+ ".airlines WHERE airline_id LIKE " + delIn);

			stmt.executeUpdate(strSelect);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	public void showFlight() { // Metod som printar ut alla flights

			System.out.println("_______________________________________________________________________");
			try{

				ResultSet result = stmt.executeQuery("select * from flight_system.flights LEFT JOIN flight_system.airports_departure"
							+ " ON flight_departure = airport_id LEFT JOIN flight_system.airports_destination"
							+ " ON flight_destination = flight_system.airports_destination.airport_id"
							+ " LEFT JOIN flight_system.airlines ON flight_airline = airline_id");
				while (result.next()){
				System.out.println("(" + result.getString("flight_id") + ") " + result.getString("flight_name") + " Departing from: "
							+ result.getString("airport_town") + " (" + result.getString("airport_name") + ") - "
						    + "Destination: " + result.getString("airports_destination.airport_town")
							+ " (" + result.getString("airports_destination.airport_name") + ")" + "\n" + "\t" + " Flying with "
						    + result.getString("airline_name") + ". Flight Date: " + result.getString("departure_date"));
				System.out.println("_______________________________________________________________________");
				}
			}
			catch (Exception exc){
				exc.printStackTrace();
			}
		}
	
	public void delFlight() { // Metod som tarbort en flight som man valt i menyn.
		try {

			int delIn = input.nextInt();
			String strSelect = ("DELETE from " + database
					+ ".flights WHERE flight_id LIKE " + delIn);

			stmt.executeUpdate(strSelect);			
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		
	}
	
	public void bookingInsert(int chooseFlight, String firstName, String lastName, int price) { // Metod som flyttar över vald data från flights till booking, för att skapa en bookning av en passagerare 
		try {
			
			
			stmt.executeUpdate("INSERT INTO flight_system.bookings (booking_firstname, booking_lastname, booking_price,"
					+ " booking_flight) VALUES('" + firstName + "', '" + lastName + "', '" + price + "', '" + chooseFlight + "');");
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
	
	public void showBookings() { // Metod för att printa ut dina bokningar.

		try{
			ResultSet result = stmt.executeQuery("select * from flight_system.bookings LEFT JOIN flight_system.flights"
						+ " ON flight_id = booking_flight LEFT JOIN flight_system.airlines ON"
						+ " flight_airline = airline_id LEFT JOIN flight_system.airports_departure"
						+ " ON flight_departure = airport_id LEFT JOIN flight_system.airports_destination"
						+ " ON flight_destination = flight_system.airports_destination.airport_id"
						);
			while (result.next()){
			System.out.println("||Flight: " + result.getString("flights.flight_name") + " || Passenger: "
						+ result.getString("booking_firstname")	+ " " + result.getString("booking_lastname")
						+ " || Price: " + result.getString("booking_price") + " SEK" + " || Flying from: "
						+ result.getString("airports_departure.airport_town") + " || To: "
						+ result.getString("airports_destination.airport_town") + " || Flying with: " 
						+ result.getString("airlines.airline_name") + " || Date: " + result.getString("flights.departure_date") + " ||");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
	
	public void delBooking() { // Metod för att kunna ta bort bokningar. 

		try {

			int delIn = input.nextInt();
			String strSelect = ("DELETE from " + database
					+ ".bookings WHERE booking_id LIKE " + delIn);

			stmt.executeUpdate(strSelect);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public void departureSearch() { // Metod för att kunna söka efter avgångar.
		
			String search = input.next();
			System.out.println("_______________________________________________________________________");
			try{

				ResultSet result = stmt.executeQuery("select * from flight_system.flights LEFT JOIN flight_system.airports_departure"
							+ " ON flight_departure = airport_id LEFT JOIN flight_system.airports_destination" 
							+ " ON flight_destination = flight_system.airports_destination.airport_id"
							+ " LEFT JOIN flight_system.airlines ON flight_airline = airline_id"
							+ " WHERE airports_departure.airport_town LIKE '%" + search + "%'");
				while (result.next()){
				System.out.println("(" + result.getString("flight_id") + ") " + result.getString("flight_name") + " Departing from: "
							+ result.getString("airport_town") + " (" + result.getString("airport_name") + ") - "
						    + "Destination: " + result.getString("airports_destination.airport_town")
							+ " (" + result.getString("airports_destination.airport_name") + ")" + "\n" + "\t" + " Flying with "
						    + result.getString("airline_name") + ". Flight Date: " + result.getString("departure_date"));
				System.out.println("_______________________________________________________________________");
				}
			}
			catch (Exception exc){
				exc.printStackTrace();
			}

	}
	
	public void destinationSearch() { // Metod för att kunna söka efter resor via destination.

		String search = input.next();
		System.out.println("_______________________________________________________________________");
		try{

			ResultSet result = stmt.executeQuery("select * from flight_system.flights LEFT JOIN flight_system.airports_departure"
						+ " ON flight_departure = airport_id LEFT JOIN flight_system.airports_destination" 
						+ " ON flight_destination = flight_system.airports_destination.airport_id"
						+ " LEFT JOIN flight_system.airlines ON flight_airline = airline_id"
						+ " WHERE airports_destination.airport_town LIKE '%" + search + "%'");
			while (result.next()){
			System.out.println("(" + result.getString("flight_id") + ") " + result.getString("flight_name") + " Departing from: "
						+ result.getString("airport_town") + " (" + result.getString("airport_name") + ") - "
					    + "Destination: " + result.getString("airports_destination.airport_town")
						+ " (" + result.getString("airports_destination.airport_name") + ")" + "\n" + "\t" + " Flying with "
					    + result.getString("airline_name") + ". Flight Date: " + result.getString("departure_date"));
			System.out.println("_______________________________________________________________________");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}

}

	public void ariportSelect() {

	}
}
