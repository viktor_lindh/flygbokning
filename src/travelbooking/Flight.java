package travelbooking;

public class Flight {
	
	private String flightName;
	private String departure;
	private String destination;
	private String airline;
	private String departureDate;
	private int id;

	public Flight(String flightName, String departure, String destination, String airline, String departureDate) {

		this.flightName = flightName;
		this.departure = departure;
		this.destination = destination;
		this.airline = airline;
		this.departureDate = departureDate;
	}

	public Flight(String flightName, String departure, String destination, String airline, String departureDate, int id) {

		this.flightName = flightName;
		this.departure = departure;
		this.destination = destination;
		this.airline = airline;
		this.departureDate = departureDate;
		this.id = id;

	}

	public String getflightName() {

		return this.flightName;
	}

	public String getDeparture() {

		return this.departure;
	}

	public String getDestination() {

		return this.destination;
	}

	public String getAirline() {

		return this.airline;
	}
	public String getDepartureDate() {

		return this.departureDate;
	}
	public int getID() {

		return this.id;
	}
}



