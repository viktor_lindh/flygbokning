package travelbooking;
import java.util.Scanner;

// Class f�r huvud menyn
public class MainMenu {
	
	 static Scanner input = new Scanner(System.in);
     // Tom konstruktor

     // metod med navigering f�r Main Menu
     public void startMenu(){
             Traveller travelMenu = new Traveller();
             Admin adminOp = new Admin();
             System.out.println("Welcome to Travelbooking!");
             System.out.println("1. Traveller\n2. Admin\n3. Exit Travelbooking");
            
             int userIn = input.nextInt();
             menuLoop:
             do {
                     switch (userIn) {
                     case 1:
                             travelMenu.travelMenu();
                             break;
                            
                     case 2:
                             adminOp.adminOptions();
                             break;
                            
                     case 3:
                             System.exit(0);
                            
                     default:
                             System.out.println("Sorry, that�s not a correct choice! Try again:\n");
                             startMenu();
                             break menuLoop;

                     }
             }while (userIn >= 2);
    
     }
}