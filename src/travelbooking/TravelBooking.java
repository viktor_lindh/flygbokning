package travelbooking;

public class TravelBooking { // Huvud class (programstart)
	
	public static void main(String[] args) {
		
		DatabaseConnector setup = new DatabaseConnector();		
		
		MainMenu mainMenu = new MainMenu();
		
		setup.databaseSetup();
		setup.ExampleData();    // ta bort kommentar om man vill ha exempel data.
		mainMenu.startMenu();
		

	}
	
	

}
