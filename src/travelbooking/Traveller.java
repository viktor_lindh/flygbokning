package travelbooking;

import java.util.Random;
import java.util.Scanner;

public class Traveller { // Traveller class med menynavigering f�r resen�rer

	static Scanner input = new Scanner(System.in);

	public void travelMenu() { // Metod f�r val p� bottenmenyn traveller.

		System.out
				.println("Travel Menu:\n1. Booking\n2. Your Bookings\n3. Main menu");
		int userIn;
		MainMenu startMenu = new MainMenu();

		do {
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				flightMenu();
				break;
			case 2:
				yourBookings();
				break;

			case 3:
				startMenu.startMenu();
				break;

			default:
				System.out
						.println("Sorry, that�s not a correct choice! Try again:\n");
				travelMenu();
				break;

			}
		} while (userIn >= 3);

	}

	public void flightMenu() { // Metod med val f�r bottenmenyn flight menu

		System.out
				.println("Flight Menu:\n1. Book your flight\n2. Search flight\n3. Back");
		int userIn;

		do {
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				travellerInfo();
				break;
			case 2:
				searchMenu();
				break;

			case 3:
				travelMenu();
				break;

			default:
				System.out
						.println("Sorry, that�s not a correct choice! Try again:\n");
				flightMenu();
				break;

			}
		} while (userIn >= 3);
	}

	public void bookingMenu() { // Metod med navigerings val f�r boknings menyn.

		DatabaseConnector showFlight = new DatabaseConnector();		
		int userIn;

		do {
			showFlight.showFlight();

			userIn = input.nextInt();
			switch (userIn) {
			case 1:

				System.out
						.print("\nEnter the number of the flight you would like to book: ");

				travellerInfo();
				break;
			case 2:
				flightMenu();
				break;

			default:
				System.out
						.println("Sorry, that�s not a correct choice! Try again:\n");
				bookingMenu();
				break;

			}
		} while (userIn >= 2);
	}

	public void travellerInfo() { // Metod f�r navigering genom p�b�rjad
									// bokning.
		String lastName;
		String firstName;

		Random randNr = new Random();
		int price;
		int bookInput;
		DatabaseConnector bookingInsert = new DatabaseConnector();
		DatabaseConnector showFlight = new DatabaseConnector();

		System.out.println("\n1. Start booking\n2. Back");

		int userIn = input.nextInt();

		do {
			switch (userIn) {
			case 1:
				showFlight.showFlight();
				System.out
						.print("\nEnter the number of the flight you would like to book: ");
				bookInput = input.nextInt();
				System.out.print("Enter Traveller info:\nFirstname: ");
				firstName = input.next();
				System.out.print("Lastname: ");
				lastName = input.next();
				price = randNr.nextInt(15000) + 1000;
				System.out.print("\nPrice for this flight is: " + price
						+ " SEK");
				System.out.println("\nConfirm your reservation:");
				System.out.println("1.YES\n2.NO");
				do {
					userIn = input.nextInt();
					switch (userIn) {
					case 1:
						bookingInsert.bookingInsert(bookInput, firstName,
								lastName, price);
						System.out.println("\nBooking complete!\n");
						travelMenu();
						break;
					case 2:
						flightMenu();

					default:
						System.out
								.print("Sorry, that�s not a correct choice! Try again: ");

						break;

					}
				} while (userIn >= 2);

				break;
			case 2:
				flightMenu();
				break;

			default:
				System.out
						.println("Sorry, that�s not a correct choice! Try again:\n");
				travellerInfo();
				break;

			}
		} while (userIn >= 2);

	}

	public void yourBookings() { // Metod f�r menyval p� dina bokningar

		DatabaseConnector showBooking = new DatabaseConnector();

		System.out.println("\n1. Show bookings\n2. delete booking\n3. Back");
		int userIn;

		do {
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				showBooking.showBookings();
				System.out.println("\n1. Back");
				backtoshizzel();
				break;
			case 2:
				showBooking.showBookings();
				System.out.println("\n1. Delete \n2. Back");
				deleteBooking();
				;
				break;

			case 3:
				travelMenu();
				break;

			default:
				System.out
						.println("Sorry, that�s not a correct choice! Try again:\n");
				yourBookings();
				break;

			}
		} while (userIn >= 4);

	}

	public void deleteBooking() { // metod f�r menyval av borttagning av dina
									// bokningar

		DatabaseConnector delBooking = new DatabaseConnector();
		DatabaseConnector showBooking = new DatabaseConnector();

		int userIn;

		do {
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				System.out
						.println("Delete an booking\nType the number of the booking you want to delete:");
				delBooking.delBooking();
				showBooking.showBookings();
				System.out.println("Deleted!");
				System.out.println("\n1. Delete \n2. Back");
				break;
			case 2:
				yourBookings();
				break;

			default:
				System.out
						.println("Sorry, that�s not a correct choice! Try again:\n");
				deleteBooking();
				break;
			}

		} while (!(userIn >= 2));
	}

	public void backtoshizzel() { // metod f�r att kunna g� tillbaka fr�n dina
									// bokningar.

		int userIn;

		do {
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				yourBookings();
				break;

			default:
				System.out
						.println("Sorry, that�s not a correct choice! Try again:\n");
				backtoshizzel();
				break;

			}
		} while (userIn >= 1);

	}

	public void searchMenu() { // Metod med meny f�r olika s�kalternativ i dina
								// bokningar

		System.out.println("Search menu:\n1. Start search\n2. Back");
		int userIn;

		do {
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				searchFlight();
				break;
			case 2:
				flightMenu();
				break;

			default:
				System.out
						.println("Sorry, that�s not a correct choice! Try again:\n");
				searchMenu();
				break;

			}
		} while (userIn >= 2);
	}

	public void searchFlight() { // Metod med s�kfunktions val i dina bokningar.

		DatabaseConnector depSearch = new DatabaseConnector();
		DatabaseConnector desSearch = new DatabaseConnector();

		System.out
				.println("Search flight:\n1. Search by departure:\n2. Search by destination\n3. Back");
		int userIn;

		do {
			userIn = input.nextInt();
			switch (userIn) {
			case 1:
				System.out.println("Where do you want to travel from?");
				depSearch.departureSearch();
				searchBooking();
				break;
			case 2:
				System.out.println("Where do you like to travel to?");
				desSearch.destinationSearch();
				searchBooking();
				break;

			case 3:
				searchMenu();
				break;

			default:
				System.out
						.println("Sorry, that�s not a correct choice! Try again:\n");
				searchFlight();
				break;

			}
		} while (userIn >= 3);
	}

	public void searchBooking() {
		String lastName;
		String firstName;
		Random randNr = new Random();
		int price;
		int bookInput;		
		DatabaseConnector bookingInsert = new DatabaseConnector();


		System.out.println("\n1. Start booking\n2. Back");

		int userIn = input.nextInt();

		do {
			switch (userIn) {
			case 1:
				System.out
						.print("Enter the number of the flight that you would like to book: ");
				bookInput = input.nextInt();
				System.out.print("Enter Traveller info:\nFirstname: ");
				firstName = input.next();
				System.out.print("Lastname: ");
				lastName = input.next();
				price = randNr.nextInt(15000) + 1000;
				System.out.print("\nPrice for this flight is: " + price
						+ " SEK");
				System.out.println("\nConfirm your reservation:");
				System.out.println("1.YES\n2.NO");
				do {
					userIn = input.nextInt();
					switch (userIn) {
					case 1:
						bookingInsert.bookingInsert(bookInput, firstName,
								lastName, price);
						System.out.println("\nBooking complete!\n");
						travelMenu();
						break;
					case 2:
						flightMenu();

					default:
						System.out
								.print("Sorry, that�s not a correct choice! Try again: ");

					}
				} while (userIn >= 2);

				break;
			case 2:
				flightMenu();
				break;

			default:
				System.out
						.println("Sorry, that�s not a correct choice! Try again:\n");
				searchBooking();
				break;

			}
		} while (userIn >= 2);

	}

}
